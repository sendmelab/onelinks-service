@extends('render')
@section('app-srv')
  <div class="content-wrapper">
    <div class="subcontent">
      <div class="img">&nbsp;</div>
      <div class="wave -one"></div>
      <div class="wave -two"></div>
      <div class="wave -three"></div>
      <div class="user"><div class="cube-info">
        @include('content/info')
@include('content/publicreg')
      </div>
      </div>
      @isset($isActive)
      <div class="nav-bar">
        <i class="fa fa-th active" aria-hidden="true"></i>
        <i class="fa fa-list-ul" aria-hidden="true"></i>
        <i class="fa fa-address-book-o" aria-hidden="true"></i>
        <i class="fa fa-bookmark" aria-hidden="true"></i>
      </div>
      @endisset
    </div>
  </div>
@stop