<!DOCTYPE html>
<html >
<head>
  <title><?=isset($t) ? $t.' - ':''?>{{ config('app.name') }}</title>
  <meta charset="utf-8"/>
  <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
  <link rel="shortcut icon" href="https://amadeuspro.network/landing/images/AmaDeuspro-fav-icon.png">
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;600;700&display=swap" rel="stylesheet"/>
    <link href="https://use.fontawesome.com/f09496b7cc.css" media="all" rel="stylesheet">
    <link href="{{ asset('aps/app.css') }}" rel="stylesheet">
    <!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">-->
    <meta name="theme-color" content="#<?=isset($ucolor) ? substr($ucolor[0],0,6):"ff006a"?>"/>
    <meta name="msapplication-navbutton-color" content="#<?=isset($ucolor) ? substr($ucolor[0],0,6):"ff006a"?>"/>
    <meta name="apple-mobile-web-app-status-bar-style" content="#<?=isset($ucolor) ? substr($ucolor[0],0,6):"ff006a"?>"/>
    <style class="INLINE_PEN_STYLESHEET_ID">
        body {font-family: 'Roboto', sans-serif;}
.content-wrapper {
  width: 100%;
  height: 100%;
  border-radius: 5px;
  box-shadow: 0 2px 30px rgba(0, 0, 0, 0);
  background: #ffffff;
  position: absolute;
  top: 0%;
  left: 0%;
  overflow-y: scroll;
  overflow-x: hidden;
  text-align: center;
}
.content-wrapper::-webkit-scrollbar {
  display: none;
}
.content-wrapper .img {
  width: 100%;
  height: <?=isset($odata) ? "350":"170"?>px;
  margin-left:auto;
  margin-right: auto;
  display:block;
  position: relative;
  overflow: hidden;
  background-image: url(https://amadeuspro.network/landing/images/countdown-3-1600x900.jpg);
  background-size: 117%;
  background-position: center;
}
.content-wrapper .img::after {
  content: "";
  display: block;
  position: absolute;
  top: 0;
  width: 100%;
  height: 100%;
  background: linear-gradient(to top, rgba(88, 81, 219, 0.25), transparent);
}
.content-wrapper img {
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  width: 100%;
  object-fit: contain;
}
.profile--info {
  text-align: left;
}
.profile--info span {
  font-family: "Open Sans", "Adobe Blank";
  z-index: 1;
  left: 15px;
  top: 15px;
  font-size: 0.575em;
  color: rgba(84, 95, 89, 0.75);
  display: block;
}
.profile--info span.username {
  font-weight: 700;
  font-style: normal;
  font-size: 1.25em;
  color: black;
}
.profile--info span.userquote {
  margin-top: -15px;
  font-size: 0.85em;
  color: rgba(84, 95, 89, 0.75);
}

.user {
  background-color: white;
  width: 100%;
  margin-top: 0;
  max-height: 600px;
  position: relative;
  padding: 0 30px;
  box-sizing: border-box;
}

.user-social-wrapper {
  display: flex;
  justify-content: space-around;
  position: relative;
}
.user-advs,.user-social-wrapper{
  text-align: center;
  margin: 20px 0;
  padding: 17px 0;
}
.user-social-wrapper::after, .user-social-wrapper::before, .user-advs::after {
  content: "";
  display: block;
  position: absolute;
  top: 0;
  width: 100%;
  height: 1px;
  background-color: rgba(241, 193, 226, 0.5);
}
.user-social-wrapper::before {
  top: initial;
  bottom: 0;
}
.user-social-wrapper .user-info {
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  font-weight: 200;
  flex: 1 1;
}
.user-social-wrapper .user-info span:first-child {
  font-size: 1.25em;
}
.user-social-wrapper .user-info span:last-child {
  font-size: 0.75em;
  color: rgba(84, 95, 89, 0.75);
}

.shots {
  width: calc(100% + 60px);
  margin-left: -30px;
  position: relative;
  display: flex;
  flex-wrap: wrap;
}
.shots .shot {
  overflow: hidden;
  position: relative;
  width: 100px;
  height: 100px;
}
.shots .shot::after {
  content: "";
  display: block;
  opacity: 0;
  transition: all 0.255s;
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background-color: rgba(0, 0, 0, 0.5);
}
.shots .shot:hover::after {
  opacity: 1;
}
.shots .shot:hover img {
  transform: translate(-50%, -50%) scale(1.25);
}
.shots .shot img {
  transition: all 0.255s;
  width: 102px;
}
.img-user > img{height:0px;}
@keyframes drift {
  from {
    transform: rotate(0deg);
  }
  from {
    transform: rotate(360deg);
  }
}
@media screen and (min-width: 830px) {
  .shots .shot{width: 33%;height: 300px;}
  .shots .shot img{width:100%;}
  .profile--image{width:100px;padding-top:-10px;z-index:9999 !important;}
  .user{z-index:9999;}
  .profile--image > div{object-fit:fill !important;height:100px;background:url(https://amadeuspro.network/landing/images/AmaDeuspro-fav-icon.png) center center/100px no-repeat;border-radius:50px;}
  .profile--info,.profile--image{display:inline-block;}
  .content-wrapper {text-align:left;}
  .content-wrapper .img{background-attachment: fixed;background-size:77%;}
}
.subcontent{background: #ffffff;}
    </style>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <!-- VARIABLE CONSTANT ON SCRIPT OPTIONS -->
    <script>var option_sb_element = [0,0],option_sb_postfix = undefined;</script>
</head>
<body>
  @if (trim($__env->yieldContent('app-srv')))
    @yield('app-srv')
  @else
  @endif
</body>
<script>
  $(document).ready(function(){
    $(".bclose").click(function(){$(".topbox").addClass("hidden");option_sb_element[0]=0;});
    $(".returnopt").click(function(){option_sb_element[0]--;});
    $(".nextopt").click(function(){option_sb_element[0]++;});
    $(".returnopt,.nextopt,.bclose").click(function(){rv_inside_fromoptions();if(option_sb_element[0]>0){$("._returnoption").removeClass("hidden");}else{$("._returnoption").addClass("hidden");}});
  });
  function rv_inside_fromoptions(){
    // OPTION PLACEMENT AUTOUPDATE FROM MAIN INTERFACE
    $(".opbase").addClass("hidden");
    $(".option_"+option_sb_element[0]).removeClass("hidden");
  }
</script>
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>-->
</html>
