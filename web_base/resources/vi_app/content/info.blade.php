@if(isset($err_msg))
<div class="user-advs">
  <i class="fa fa-exclamation" aria-hidden="true" style="font-size:60pt;"></i><br/>
  <h2>{{ $err_msg[0] }}</h2>
  <p>{{ $err_msg[1] }}</p>
</div>
@elseif(isset($odata))
<div class="profile--image">
  <div>&nbsp;</div>
</div>
<div class="profile--info">
  <span class="username">Sample User</span><span>@sampleuser</span><br><span class="userquote">Sample Frame</span>
</div>
<div class="user-social-wrapper">
  <div class="user-info user-posts"><h2>0</h2><span>Prensa</span></div>
  <div class="user-info user-followers"><h2>0</h2><span>Social</span></div>
  <div class="user-info user-following"><h2>0</h2><span>Musica</span></div>
</div><a href="#">Seguir</a>
<div class="shots">
  <div class="shot"><img src="https://i.imgur.com/p96Dx5N.png"></div>
  <div class="shot"><img src="https://i.imgur.com/DqQYs9C.png"></div>
  <div class="shot"><img src="https://i.imgur.com/0slF26H.png"></div>
  <div class="shot"><img src="https://i.imgur.com/tfwlQuG.png"></div>
  <div class="shot"><img src="https://i.imgur.com/mx505rF.png"></div>
  <div class="shot"><img src="https://i.imgur.com/ahqJFvv.png"></div>
  <div class="shot"><img src="https://i.imgur.com/9IxlD6e.png"></div>
  <div class="shot"><img src="https://i.imgur.com/0b9Jkaj.png"></div>
  <div class="shot"><img src="https://i.imgur.com/AoMm0Ov.png"></div>
</div>
@else
  @include('content/started')
@endif