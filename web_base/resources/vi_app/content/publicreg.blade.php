<div id="_mainsys" class="topbox hidden">
	<div class="upper-box">
		<div class="user-advs base-advs">
			<h3>Registro</h3>
			<a href="#!" class="bclose">&#10006;</a>
		</div>
		<div class="mixed_box">
			<div class="user-advs inp-onelinked">
				<div class="opbase option_0">
					<p>Seleccionar tipo de cuenta de usuario publico:</p>
					<div class="centrebox">
						<i class="fa fa-newspaper-o" aria-hidden="true" style="font-size:60pt;"></i><br/>
						<label class="baselabel">Prensa
  							<input type="radio" name="account_type">
  							<span class="checks"></span>
						</label>
					</div>
					<div class="centrebox">
						<i class="fa fa-users" aria-hidden="true" style="font-size:60pt;"></i>
						<label class="baselabel">Social
  							<input type="radio" name="account_type">
  							<span class="checks"></span>
						</label>
					</div>
				</div>
				<div class="opbase option_1 hidden">
					<p>Para finalizar tu registro es necesario que ingreses tus datos a continuación, <strong>es probable que necesitemos verificar tu cuenta mediante correo electronico,</strong> una vez confirmado podras ingresar tu <strong>Nombre de Usuario</strong></p>
					<div class="centrebox">
						<input type="text" name="fullname" maxlength="40" placeholder="Nombre Completo"><br/>
						<input type="number" name="phone" min="200000000" max="999999999" placeholder="N° Telefono" /><br/>
						<p>Genero:</p>
						<select name="gender">
							<optgroup label="Define tu genero">
								<option value="0">Masculino</option>
								<option value="1">Femenino</option>
								<option value="-1">Prefiero omitir</option>
							</optgroup>
						</select><br/>
						<input type="email" name="mail" maxlength="80" placeholder="Correo Electronico">
					</div>
				</div>
			</div>
			<div class="option_box inp-onelinked">
				<div class="hidden _returnoption"><input type="button" value="Atras" class="returnopt" /></div><div><input type="button" value="Seguir" class="nextopt" /></div>
			</div>
		</div>
	</div>
</div>