<style>@media screen and (min-width: 830px) {.wave{display: none !important;}}</style>
<link href="{{ asset('aps/inp_app.css') }}" rel="stylesheet"/>
<div class="user-advs">
  <h1>OneLinks</h1>
  <p>Bienvenido a nuestra plataforma, inicie sesión o registrese para continuar</p>
  <div class="inp-onelinked">
    <form id="_loginstart" method="post">
      <input type="text" maxlength="25" name="user" placeholder="Usuario" /><br/>
      <input type="password" maxlength="40" name="pwd" placeholder="Contraseña" /><br/>
      <a href="#recover" id="recover">¿Olvidaste la contraseña?</a><br/>
      <!--<br/>-->
      <input type="submit" value="Entrar" />
    </form>
    <hr/>
    <i class="fa fa-users" aria-hidden="true" style="font-size: 50pt;margin-top:20px;"></i><br/>
    <h2>Eres nuevo en nuestra plataforma</h2>
    <a href="#register" id="register">Registrate aqui</a>
  </div>
</div>
<script>
  $(document).ready(function(){
    if (window.location.hash.substr(1) == "register") {_register_startedoptionfix();}
    $("#register").click(function(){_register_startedoptionfix();});
  });
  function _register_startedoptionfix(){
    $("#_mainsys").removeClass("hidden");option_sb_element[1]=2;
    option_sb_postfix = function(){
      if (option_sb_element[0]==0) {
        
      }
      return false;
    };
  }
</script>