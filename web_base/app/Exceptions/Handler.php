<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }
    public function render($request, Throwable $exception)
    {
        if ($this->isHttpException($exception)) {
            switch($exception->getStatusCode()){
                case 403:["err_msg" => array("Sin Servicio","El servicio solicitado no se encuentra disponible")];
                break;
                case 404:
                $inMsg = ['err_msg' => array("Servicio no encontrado","No se encuentra el servicio solicitado")];
                break;
            }
            if (isset($inMsg)) {
                return response()->view('base',$inMsg);
            }else{
                return response()->view('base');
            }
        }else{
            return parent::render($request, $exception);
        }
    }
}
