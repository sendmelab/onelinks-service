<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use League\ColorExtractor\Palette;
use League\ColorExtractor\Color;
use League\ColorExtractor\ColorExtractor;
class Land extends Controller
{
    public function index(Request $smc,$user = null){
        $rm_out = array("title"=>"Iniciar Sesión");
        $ht_base = array("isActive"=>$smc->session()->get('active_section'),"t"=>$rm_out["title"]);
        $usearch = isset($user) ? $user:$smc->session()->get('active_section');
        if (isset($usearch)) {
            //BUSCAR POR SESION O USUARIO RAIZ
            if (isset($rm_out["urequest"])) {
                $ht_base["odata"] = $rm_out["urequest"];
                $ht_base["t"] = strtolower($rm_out["urequest"]["name"]);
            }else{
                $ht_base["err_msg"] = array("\"".strtolower($user)."\" no existe","Es probable que la persona al que intentas buscar se haya cambiado el nombre o no se encuentra en nuestros registros.");
                unset($ht_base["t"]);
            }
        }
        //GENERATE HEX COLOR WITH IMAGE FILE
        $palette = Palette::fromFilename('https://amadeuspro.network/landing/images/countdown-3-1600x900.jpg');
        $extractor = new ColorExtractor($palette);
        $ht_base["ucolor"] = $extractor->extract(5);
        return view("base",$ht_base);
    }
}
